package com.StepDefination;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginSteps {
	WebDriver driver;
	
	
	
	
	@Given("user login page")
	public void user_login_page() {
		System.out.println("Step 1");
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://opensource-demo.orangehrmlive.com/index.php/auth/validateCredentials");
		
	    
	}

	@When("user enters username and password")
	
	public void user_enters_username_and_password() {
		System.out.println("Step 2");
		driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		driver.findElement(By.name("txtPassword")).sendKeys("admin123");
	   
	}

	@And("click on login")
	public void click_on_login() {
		System.out.println("Step 3");
		driver.findElement(By.id("btnLogin")).click();
	    
	}

	@Then("user should login on home page")
	public void user_should_login_on_home_page() {
		System.out.println("User is on home page");
		driver.close(); 
	}

}
